# Todo App RESTful API

This is a simple server app for managing todos built on Spring Boot and PostgreSql

## API routes

### GET /api/todo

Return list of todo

### GET /api/todo/{id}

Return a todo by id

### POST /api/todo

Add new todo

### PUT /api/todo/done/{id}

Complete todo

### DELETE /api/todo/{id}

Delete todo
