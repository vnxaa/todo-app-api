package com.example.apitodo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
@Repository
public interface ToDoRepository extends JpaRepository<ToDo,Integer>{

}
