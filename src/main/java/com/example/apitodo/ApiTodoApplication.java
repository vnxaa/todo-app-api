package com.example.apitodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.context.annotation.Bean;
@SpringBootApplication
public class ApiTodoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(ApiTodoApplication.class, args);
        ToDoRepository toDoRepository = context.getBean(ToDoRepository.class);
        toDoRepository.findAll()
                .forEach(System.out::println);
    }
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//
//                registry.addMapping("/api/todo").allowedOrigins("http://localhost:3001");
//
//            }
//        };
//    }

}
