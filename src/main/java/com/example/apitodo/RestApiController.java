package com.example.apitodo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RestApiController {
    @Autowired
    private ToDoRepository todoRepository;

    @CrossOrigin
    @GetMapping("/todo")
    public List<ToDo> getToDoList(){
        return todoRepository.findAll();
    }
    @CrossOrigin
    @GetMapping("/todo/{todoId}")
    public Optional<ToDo> getTodo(@PathVariable(name = "todoId") Integer todoId){
        return todoRepository.findById(todoId);
    }
    @CrossOrigin
    @PutMapping("/todo/done/{todoId}")
    public ResponseEntity doneToDo(@PathVariable(name="todoId") Integer todoId){
        Optional<ToDo> todo= todoRepository.findById(todoId);
        ToDo doneTodo = todo.get();
        doneTodo.setStatus(true);
        todoRepository.save(doneTodo);
        return ResponseEntity.ok().build();
    }


    @CrossOrigin
    @DeleteMapping("/todo/{todoId}")
    public ResponseEntity deleteTodo(@PathVariable(name="todoId") Integer todoId){
        todoRepository.deleteById(todoId);
        return ResponseEntity.ok().build();
    }
    @CrossOrigin
    @PostMapping("/todo")
    public ResponseEntity addTodo(@RequestBody ToDo todo){
        todoRepository.save(todo);
        return ResponseEntity.ok().body(todo);
    }
}
