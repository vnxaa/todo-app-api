package com.example.apitodo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table (name="todo")
@Data
public class ToDo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "status")
    private boolean status;
}
